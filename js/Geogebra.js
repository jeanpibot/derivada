var parameters = {
    "id": "ggbApplet",
    "width": 1000,
    "height": 500,
    "showMenuBar": true,
    "showAlgebraInput": true,
    "showToolBar": true,
    "customToolBar": "0 39 59 | 1 501 67 , 5 19 , 72 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49 50 , 71 | 30 29 54 32 31 33 | 17 26 62 73 , 14 66 68 | 25 52 60 61 | 40 41 42 , 27 28 35 , 6",
    "showToolBarHelp": true,
    "showResetIcon": false,
    "enableLabelDrags": false,
    "enableShiftDragZoom": true,
    "enableRightClick": false,
    "errorDialogsActive": false,
    "useBrowserForJS": false,
    "allowStyleBar": false,
    "preventFocus": false,
    "showZoomButtons": true,
    "capturingThreshold": 3,
    // add code here to run when the applet starts
    "appletOnLoad": function (api) {
        /* api.evalCommand('Segment((1,2),(3,4))');*/
    },
    "showFullscreenButton": true,
    "scale": 1,
    "disableAutoScale": false,
    "allowUpscale": false,
    "clickToLoad": false,
    "appName": "graphing",
    "showSuggestionButtons": true,
    "buttonRounding": 0.7,
    "buttonShadows": false,
    "language": "es",
    // use this instead of ggbBase64 to load a material from geogebra.org
    // "material_id":"RHYH3UQ8",
    // use this instead of ggbBase64 to load a .ggb file
    // "filename":"myfile.ggb",
    "ggbBase64": "UEsDBBQACAgIABZLU08AAAAAAAAAAAAAAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmltv2zYUgJ/XX0HwaXuILcmW7QRRirTAsAJpGixBsVdaomUuFKmRVCzn148iZV3qyxLFrZ0ueQh9KF7E7xweHpI6f58nFDxgIQlnAXR7DgSYhTwiLA5gpmYnE/j+4t15jHmMpwKBGRcJUgH0i5JVPS31/JFX5KE0DWAsUDrXTUCQUqSKOgFcQABySc4Yv0YJlikK8W04xwm64iFSppm5UulZv79YLHqrDntcxP04Vr1cRhDol2UygOWPM91cq9JiYIp7juP2//p8ZZs/IUwqxEIMgR5IhGcoo0rqn5jiBDMF1DLFAUw5YQoCiqaYBvCmkMCvM4HxbxCUlTQfB168++VczvkC8OnfONR5SmS4qmeEflFGP/7IKRdABNBzIYiL2jqd6vTU05xoOkcBdGxpipZYgAdEqxyUKR6aBkzuDFGJV2V1V595hO2TYVmekcRgBFJhrQKnpzuTKcaR7hCWg3SNQpZGuY0WQ85FJEEewGt0DcGyTB9taooYPLfksezUb+aqJcWNdz/vl2SfxjjCKWaRLtQC7XYCfTo2oItkapM3zDsxj14J5gNAdp9P+QtrsvXefMV+jfgT+xPH+q2bkAevBvKrQNy24OH/E64pYjHK4n8AQ56kFOd7ZE8JqzleGaHi7nWLMhyD3THQnQM5Dacz9AKIxafmJLxnWMqCbd1u8eMPEuklzPTHdQBJlG7JHU9sC/gf1lIa0TojusxuRcwyFirjVEq4HzPx0NTGYOgcQh91m51nwBZldCbtOcPdLCWOC6nicruSa9PuFte9mXbbtHFem7YmlSlavNcnpvSGDBtzlmtDv8c4vdMdfWF3AjFZbMralrZdrwItd+nUfw06fdPoJq93/RWJSk+Z3h/M9MiipnK7BVhN5Xq+X6u35/mH1vAz1oKNRLpFRUdr7s+05f2Y1aibzyhWoI0Ye+MjNqsHPTxe8/hainWE8SrivSNzoBuCdCQUlgSx/9r10GXcmOM3K7nSx9jq46Vx0n53Tj3XsX/u8NRx3ZHrHT762Q25tcW5qTJqzO7LMFemcOQTZx88Q85IWG9RrFSRHP5kDmQP4RWJMbN+VwKQO6bY0jGVH53yoiN3jbx0zdNH12ab+vrFBcnBpa1xaQteejYZ2GRoE78C1G0DalSbat/VCLG/WSCG3XZNx+FOftzG9AjV/gNCeZYlWDScw/VKrszHt+5Bt5fhlmqf4Ay22cl2q5CURNqEEqKVdKK1l6DcaBFNJaeZwrehwJjVl37WkBckUvNCk7rvGckLc7FtgjkX5JEzVdEAxTy4pOZ6sHVOssl8vF2BbMtYX+agEYtpPR8vrVRrwN4FmELfnhFuUkyToVMiHPW8ycCd+ANn7I5P/cnoiUjdSY3UPngy0ZbDKdXRZUX5fs7mWS7Da64U/qDsUYT1We7A2bNZrO0of68y6r3QMR4vGoNZK/rdTg4pDzNZn4dbqSI0+cniG5TlhBIklus9vTDy2U5Y4byOMO6M0Pis4QgBbx+Kxh7Xr/bJSo1PB+xgZkRTZCjRFWwnhH1A4X0seMai9VVrL0N3D21b26FNOacY1Y7ow0puXFivxQnbAL1gLdgXoXCOw/spz1tL224fQ2Q9A66M0LhH3jADnj7KYgK1x3lycFPocrK37XJzY+DSJN1vfFfVX327dfEvUEsHCPIRFYnhBAAAXiYAAFBLAwQUAAgICAAWS1NPAAAAAAAAAAAAAAAAFwAAAGdlb2dlYnJhX2RlZmF1bHRzM2QueG1s7ZjdbtMwFMev4Sks39PaaZIt0zJUjQuQNgTihlsvOW0NiZ3Z7trs1XgHnokTO+tSWNFWbZOG6EWPv85x/Ps7p3aP367rilyBsVKrnPIRowRUoUup5jldutmbQ/r25PXxHPQcLowgM21q4XKadCM3flgbJWnUtYmmyenciGaBIShpKuE6n5yuKCFrK4+U/ihqsI0o4EuxgFqc6UI4H2bhXHM0Hq9Wq9HNhCNt5uP53I3WtqQEH1bZnPaFIwy35bSa+OERY3z89fwshH8jlXVCFUAJLqSEmVhWzmIRKqhBOeLaBnJaaCWLCc5RiQuocvpBOVwdFN2TkWJprtC/d87phCeMnrx+dWwXekX0xTccl1NnlrDx95VxNwa7T3WlDTE5jTglCJYztBdoswiJVc1C5JSNOAsfHmeM85RHwb8SLRhyJTAoCy1i6XThQ/rWmags3IzFyc91CaEn7scrWXvExDpAeXBy2wCUvhSWz7xUrZd9GE8q+OLaCohbyOK7Aov4k4FTV3gvyxK63RN8QM5BXSERbSxKzvwsLfPDr1m/ydbc11vue695aPb++KhGrsk0eEzDwGkUzCSYOJhkgwQuVXhO233ntBEGdxkGKrr+43Ev9h+yi7W0A9WnXfXdltJsspfSzAvNvMzsVuSnkhR3z9OKupsv6cuAq/754++4/YtUCOPASqEG4E+7jt/Jpy+B/FNy3w0S4ysY8Pvk61v8MA3uxS/LPMCIZx6ht5sclTwWxkJrU1qyDmkgJAf/vdqEnInuZ6ifZWd2vAsq2xOqrtoFlEarW66Dplu0kx7tPm/So/5kxD2UJEtZnMaPps6+m/xBbKemWMgaShDbcFHa54Mb6MYHHm5n/g22n1rMybLc5vqcmzYkkSxwjdg/w9VIW29T5c9INQ2pOVDN0hdJVYHbrPNjVx7m1eR/Xn0YzculKP0prF/s55v6kCrf87KyOzmmcdZ9DlKeHPI44o8F6CkuHHdeN7rGcKdog7mONgEfegMh0zSYg2AOg8l23k5k3VSykO7v0tqlmeEt+a7jct+1rXK8n8rod+eBeXRw321/G/hZjsz8vqe78eC2P775R+HkF1BLBwi/dwR7NAMAAPQQAABQSwMEFAAICAgAFktTTwAAAAAAAAAAAAAAABYAAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzSyvNSy7JzM9TSE9P8s/zzMss0dBUqK7lAgBQSwcIRczeXRoAAAAYAAAAUEsDBBQACAgIABZLU08AAAAAAAAAAAAAAAAMAAAAZ2VvZ2VicmEueG1s3Vvrcts2Fv7dPgWGP7r21pJx4bWV2rHT607azWyyO+1Opx2IhCTGFKmSlC03yVv0NfYp9n+fac8BSIq62ZLlpMomkSiC4AHO950bQKb3+XySkGuVF3GW9i3WpRZRaZhFcTrqW7Ny2PGtzz/7sDdS2UgNckmGWT6RZd9ysGdzH5x1HZdjm5xO+9Yol9MxiLDINJEl3tO3biwSR33L5jYNhhHtONT1O/aQik7ghaLDlXCGA+o5gS8sQuZF/EmafS8nqpjKUD0Px2oin2ahLPWA47KcfnJ+fnNz062n1s3y0floNOjOi8gioFZa9K3qxycgbummG6G7c0rZ+Q/fPTXiO3FalDINlUVQ5Vn82Ycf9G7iNMpuyE0clWMASLiuRcYqHo0RhACmeo69poDEVIVlfK0KuLd1qpUuJ1NLd5MpXv/A/CJJo49Fovg6jlTet2iX2a7NuO/bQgifM+FbJMtjlZZVZ1YNel6L613H6sbIxV8GZxp4wFBcxINE9a2hTArQK06HOWDanBflbaIGEoYt8xmcL2bEzuAvdIh/UygMmDVI9C2Y1Vlgn3mUnjkONXNpD2yRMssSLZQSJyCvXxNOOSVneGDmwOHguuYSNW1UmAM3B9scHNPHNrfbpqtt+timjy320bNqWFK0VlO01WSgH35c+Gj9V/T0W3oyVOI1YTh7fRAE5830/PFgV6euOfX0gVFzYNVFH780Xu6BGokHacRaoxpz2GfQekjG0EV2HZMfMmajJt+kJne2qHkguo2iTmtQGEv/05+1IQU/yBUfMKK75IbvenSIXo+Dse3vPKZHlwJPHXXMkVXHd87DocGpAWL3IR3Gl5EgjDgQnxzCAgy7GHw4YQ6xocWHFo8IbHOYTQTBLkwQHVttHadcuIKX4RviPWEYgwFLAkEccOUYth2HONDNw3sxArqBlgfhX/eGGcFHYJsQ8NFtwoYPBnIHBDlGDMzDEa7+hYHTAfkOckh0o/CJHcBA2OB4jAiYA5x7lIBEgeKZ1gOSBf5jxOQJj3CfgDxQHSXTvWzg4abZO6+zcq8igxRj7FuFuVJNCqRHBDpJViS5iGHFlMdbTJ0hV66zoAvJ8pfocvxlzlxs9EySIRpuQx63a/7OKgZfrzEIgNstzDH9emgFFeYwOG+jzl3icuIhiWBFkNldH+cOQjkBvly8dQsnUCNmRdyAO1bJtIFd4xin01m5hF04ieqfZQa9ZaIrwKp/lIVXlw3alSQli7ItFoqnRY1miqmlEu6DXiIHKoEa9jnaAiHXMsGEo0cYZmlJ6ljALS1Ol4s9NQuTOIpl+i/gvq7Mvp9NBion+meGSmoheDtp6kq2WlfqPmGW5dHz2wJshcz/rfIMAwHv2ox5UCIKKnwb4vytuQK+1vV9ods922NoqaFEK+dOV/iBTx0eBMJzPLxnyyXqmpHV9XNVlqB+QeRcFTXcozyO2r+/LS6zJGqAnWZxWj6R03KW6wUEBL8cVbpIR4nSQGqOodoOrwbZ/LlJ3q6R9eJ2qjBX6fEHoydZkuUkxyk60KE6DsxR98GJNb2o7kN1j0oGCm2us4DrHvo4MEfdCzg2U6sUZbWWjNbDxIUOrCC8bYXaQLB0n6Vx+bQ+KePwaqEp9jf01xAui2SPJLJ3vmJ5vconajucZJEyNsxq00oSOS1UZFo9I2Xprt6VylOVmN4pGMMsmxWmu+FbC5oV6pksxxdp9A81Ai9+JjGUljA903WhdaTCeAI3mvYKfomm8U9Q17RGapSrGiUzGUOOvooMTHMlo2KsVNkQbdyk3c14WClzMD0YE6PL8mx657VyvVJCKtDZcxJDDOqAfUzkXJem4HjTCrFeEebxFD2CDCAbXKmF1UdxgSKiFiwIWAFjhXrkMi6RPPANlcP0SxliKyxcAac8DiUBbhL8zuNrGUEck7NynIHRqmgm8ygjYZaXqoCRZIlG4JG/gYFkEMSZsDDqJGoCy0NSag9KZxMQFDbWNNZLT9B+VsPTtSuAwJRINngJUW/FAhekweUtPkZkMh1LvWCtPEneYphrMaClfbdqekWCK90KbadCm6HAQZElsxJW+2AD6WK1b6ZWh0pMsHCDHUAUg1MffGIYz1UTlwC5+Dew14Zu7TIXxpiWTHfh+uUYfAwW04Wmvawikf7xTRxFKm0mLxt7MsaB2oNRKmW8ubl1CmjoGNhyUUMTEjYHMyhw76QGfAgpao6J72R+Svrk5GROOoSf/nzCT8lfoTY4J94p+ZiIeoThLNW2pSUv899cWsi+m+w6RV7j4v1O0nUIrmnfkXS6Gbd7ULuPJa++iTkbaGpjHWaTiUwjkuoC7xlmKGtRVkiK+BilZ2XdcmGEVLeuIazTXAPfxT3w3u9LB8CqLX810Op4WGgn6dqCcqihqSsgy7ue9hnR9W3hCQ5Vh/CYC470W2PfWjdMzEao025dRNg1lNct+kll0Sfzk4vTM0JPrWX01g13GdYnfyas9yFI3wpklzVk4zMyPBmf7gvZ5XFAhvnFmJlwYZ1mc7/+fiuofbEwtMuHGNoXx4XaY5jWcsx7rkbYvhL1wNgkQ+1Xg5+8O/gVlbQaHrlTdtmCX13T08Px22RtDZwd1hW2u7jEbHFHcnFqCxKbawD1a2ruKUxVDuaXxGFcNlAmmMS+xXKvUDoXr5fNV0pNccXz9/RFLtMCn5GsVqj78Xlh+HyyxudgPz4HR8PnapJqsel1Kfc9Cos46jIuvPeXzKcw581MXq4xGd7NJKrf0BQ+jEbGvbcQ2DyfMs8XFBzUpjYKBS47dpeuplaMfODIHjhr4AK3QeB6zi7kst3IjScLcrdz8gIWpVsdbL1ajPagJXoYLfUeiKbncfyrQ7tuYAvHA+diDCAXJv8w42K06wH0DrcdZlMKrri2XXIADW0fuzu5f1knd3pGbqGQ3De5f3kkyf3d1d1fLSF2uTdiXx0XYm+hhNwlm35pnP1izdnVftlUHUk2NXBuWMz8n6XTzWx+tS2jjvZjc3RUbOoVw/tf3K6HsFLNy4xVceyjX2dZ+Snui/V/GuYyfMXevPLe/JSoYXlC5h1OyE/6GcMp+fkVf/OxMP037IuhVGtliAfR6TCTix1xGJ9x8VS+UD+s7t7op0uFyuPh4nkkxLjv8PUsu3766Fg1P/WmKm52690tYoquQHjc5W5AKViFFzCT3J0u9TgLPM59XWMtwulDStT5xTwu1pwq3qMeih9GAU58ZA4Dc3gcl6qKn7VEvd2d7Lu3ix9WgG5AvFqu325E/OUeiL88FsQ7rLWka55JvDuQmxC2gnRskH65hvLXd6O8XEZ9fSRlVLMXt3PpKR5URj3LkttRlm5byhLJKwCXIJ1myX//A7dhtMeOo1+Y6Sr1D9G3Br+w+2A3A9fAtkTenwM329MjZoQud/ahbikv3l2K/PInZy85m8dJLPPbtez/jpf+x1LG3L1L+j7Q1dnGVx2jWcA49ZjgUFkELvN32hN4L/gZvA/81Iu4Tb7TgdLORnoCCsS5wg12WgQcCTl7bHCvJ5E/fp/vt4zDGw5YyK29AHFYzbktGO7D5z0vLRwln5fb+fxxXz5/PAo+HzV+HhulW5bpfGmZ/sfvt6RPzG/yMQFi4NucVuvyDfuP6wtz/ifSuefyu/2u2WL5zWD9bduBAFsIHNsLAvP4yusyGjDfZ4Fvwx8mNi+/t0AtVqCeL0M9fxDU4n2H2ukGQgjXY54jAu5XDwp51+Wuh0se22c2d/xdNjq+/0K/gYevAN/7itDwIznNik/vDlTrr2L95aB3sXSVgdUG5Ychf3dRwDeFnH0fl11ue1x2tcdmxdVhj8seL1N37eXHEOi67f0ix/aXOuwC88ZX13Z/WHbefsdUv4Je/U/Iz/4HUEsHCCQFqdFYCwAA1jkAAFBLAQIUABQACAgIABZLU0/yERWJ4QQAAF4mAAAXAAAAAAAAAAAAAAAAAAAAAABnZW9nZWJyYV9kZWZhdWx0czJkLnhtbFBLAQIUABQACAgIABZLU0+/dwR7NAMAAPQQAAAXAAAAAAAAAAAAAAAAACYFAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbFBLAQIUABQACAgIABZLU09FzN5dGgAAABgAAAAWAAAAAAAAAAAAAAAAAJ8IAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzUEsBAhQAFAAICAgAFktTTyQFqdFYCwAA1jkAAAwAAAAAAAAAAAAAAAAA/QgAAGdlb2dlYnJhLnhtbFBLBQYAAAAABAAEAAgBAACPFAAAAAA=",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {
    'is3D': 0,
    'AV': 1,
    'SV': 0,
    'CV': 0,
    'EV2': 0,
    'CP': 0,
    'PC': 0,
    'DA': 0,
    'FI': 0,
    'macro': 0
};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function () {
    applet.inject('ggbApplet')
};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=',
    'https://www.geogebra.org/images/GeoGebra_loading.png',
    'https://www.geogebra.org/images/applet_play.png');